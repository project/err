<?php

/**
 * @file 
 * Error code administration.
 * @author Tj Holowaychuk <tj@vision-media.ca>
 * @link http://vision-media.ca
 * @package err
 */

/**
 * Display error codes.
 * 
 * Page callback for admin/settings/err/codes.
 * 
 * @todo theme func
 * @todo severity labels
 * @todo fetch number of errors of each code current logged
 */
function err_display_codes() {
  $output = '';
  $rows = array();
  $header = array(t('Module'), t('Code'), t('Message'), t('Description'));
  $modules = err_invoke();
    
  foreach((array) $modules AS $module => $codes){
    if (!empty($codes)){
      // Module heading
      $rows[] = array(
          'data' => array(
              array(
                'data' => $module,
                'colspan' => 4,
              )
            ),
          'class' => 'module',
        );         
        
      // Codes
      ksort($codes, SORT_NUMERIC); 
      foreach((array) $codes AS $code => $code_info){
        $rows[] = array('', $code, t($code_info['message']), t($code_info['description']));
      }
    }            
  }
  
  $output .= err_display_code_search();
  $output .= theme('table', $header, $rows);
  
  return $output;
}

/**
 * Display err settings form.
 * 
 * Page callback for admin/settings/err/settings.
 */
function err_settings() {
  $form = array();

  $form['err_message_suffix'] = array(
      '#type' => 'textfield',
      '#title' => t('Message Suffix'),
      '#description' => t('Optional message suffix such as "Please contact site administrators".'),
      '#default_value' => variable_get('err_message_suffix', ''),
    );  
  
  return system_settings_form($form);
}

/**
 * Display code search JavaScript. 
 */
function err_display_code_search() {
  drupal_add_js(drupal_get_path('module', 'err') . '/err.js');
  
  return '
      <div id="err-code-search">
        <label>' . t('Code') .':</label> 
        <input id="code" type="textfield" />
      </div>
    ';
}
 