
var Err = Err || {};

/**
 * Initialize.
 */
Err.init = function() {
  $('#code').keyup(function(){
    var val = $(this).val();
    Err.filterByCode(val);
  });
};

/**
 * Filter table rows by error code.
 *
 * @param int code
 */
Err.filterByCode = function(code) {
  $('table tbody tr:not(.module)').hide().addClass('hidden');
  $('table tbody tr td:contains(' + code + ')').parents('tr').show().removeClass('hidden');
  Err.restripeTable($('table'));
};

/**
 * Re-stripe a tables rows.
 *
 * @param object node
 *   Table node.
 */
Err.restripeTable = function(node) {
  $('tbody  tr:not(.hidden)', node)
  .removeClass('odd')
  .removeClass('even')
  .each(function(i, tr){
    if (i % 2)
      $(tr).addClass('even');
    else
      $(tr).addClass('odd');
  });
};

if (Drupal.jsEnabled) {
  $(function(){
    Err.init();
  });
}